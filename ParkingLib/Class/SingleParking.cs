﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ParkingLib.Class
{
    public class SingleParking
    {
        private static Parking instance;
        private static object syncRoot = new Object();
        private SingleParking()
        { }

        public static Parking getInstance()
        {
            if (instance == null)
            {
                //lock (syncRoot)
               // {
                    instance = new Parking();
               // }
            }
            return instance;
        }
    }
}
