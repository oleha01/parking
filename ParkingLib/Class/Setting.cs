﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ParkingLib.Class
{
   public static class Setting
    {
        static public readonly double InitialBalance = 0.0;
        static public readonly int MaximumCapacity = 10;
        static public readonly int Periodicity = 5;
        static public readonly double Fine = 2.5;
        static public readonly double PriceLightCar = 2;
        static public readonly double PriceTruck = 5;
        static public readonly double PriceBus = 3.5;
        static public readonly double PriceMotorcycle = 1;
        static public double getPrice(CarEnum car)
        {
            int i = (int)car;
            switch (i)
            {
                case 1:return PriceLightCar;
                case 2: return PriceTruck;
                case 3: return PriceBus;
                case 4: return PriceMotorcycle;
                default:return -1;
            }
        }
    }
}
