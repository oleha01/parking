﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ParkingLib.Class
{
  public  class Car
    {
        public CarEnum CarType { get; set; }
        public double Balance { get; set; }
        public string LicensePlate { get; set; }
        public Car(double Balance , CarEnum CarType,string LicensePlate)
        {
            this.LicensePlate = LicensePlate;
            this.CarType = CarType;
            this.Balance = Balance;
        }

        public void MinusBalance(double money)
        {
            Balance -= money;
        }

    }
}
