﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ParkingLib.Class
{
    public class Parking
    {
        public double Balance { get; set; }
        public int Capacity { get; set; }
        public List<Car> Cars { get; set; }
        public List<Transaction> Transactions { get; set; }

        public Parking()
        {
            Cars = new List<Car>();
            Transactions = new List<Transaction>();
            this.Balance = Setting.InitialBalance;
            this.Capacity = Setting.MaximumCapacity;
        }

    }
}
