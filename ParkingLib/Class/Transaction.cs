﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ParkingLib.Class
{
 public   class Transaction
    {
        public DateTime Date { get; set; }
        public string LicensePlate { get; set; }
        public string CarType { get; set; }
        public double Amount { get; set; }
        public Transaction(CarEnum CarType, double Amount, string LicensePlate)
        {
            this.LicensePlate = LicensePlate;
            Date = DateTime.Now;
            this.CarType = CarType.ToString();
            this.Amount = Amount;
        }
    }
}
