﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Parking
{
    static class Menu
    {
        static public int CheckNumber()
        {
            int i = 0;
            try
            {
                i = int.Parse(Console.ReadLine());
            }
            catch
            {
                Console.WriteLine("Не вірно введено номер, спробуйте ще раз(Введіть '0' що повернутись назад): ");
            }
            return i;
        }
        static public int MainMenu()
        {
            Console.Clear();
            string s="";
            s += "1.Дізнатися поточний баланс Парковки\n";
            s += "2.Суму зароблених коштів за останню хвилину.\n";
            s += "3.Дізнатися кількість вільних/зайнятих місць на парковці.\n";
            s += "4.Вивести на екран усі Транзакції Праковки за останню хвилину\n";
            s += "5.Вивести усю історію Транзакцій\n";
            s += "6.Вивести на екран список усіх Транспортних засобів \n";
            s += "7.Створити/поставити Транспортний засіб на Парковку\n";
            s += "8.Видалити/забрати Транспортний засіб з Парковки\n";
            s += "9.Поповнити баланс конкретного Транспортного засобу\n";
            Console.WriteLine(s);
            Console.WriteLine("Виберіть дію: ");
            int i = CheckNumber();
            
            return i;
        }
        static public (string,int,double) CreateCarMenu()
        {
            Console.Clear();
            string s = "";
            s += "\tДобавлення транспортного засобу на парковку\n\n";
            s += "Введіть номерний знак автомобіля: ";
            Console.Write(s);
            string LicensePlate = Console.ReadLine();
            Console.WriteLine();
            Menu.SelectType();
            Console.WriteLine("Виберіть тип автомобіля: ");
            int carType = CheckNumber();
            Console.Write("Введіть почтаковий баланс: ");
            double Balance = CheckNumberDouble();
            return (LicensePlate, carType,Balance);
        }
        static public void SelectType()
        {

            string s = "";
            s += "1.Легковий\n";
            s += "2.Вантижний\n";
            s += "3.Автобус\n";
            s += "4.Мотоцикл\n";
            Console.WriteLine(s);
        }
        static public string DeleteCarMenu()
        {
            Console.Clear();
            string s = "";
            s += "\tВидалення транспортного засобу з парковки\n\n";
            s += "Введіть номерний знак автомобіля: ";
            Console.Write(s);
            string LicensePlate = Console.ReadLine();
            return LicensePlate;
        }
        static public (string,double) AddBalance()
        {
            Console.Clear();
            string s = "";
            s += "\tПоповнення балансу транспортного засобу з парковки\n\n";
            s += "Введіть номерний знак автомобіля: ";
            Console.Write(s);
            string LicensePlate = Console.ReadLine();
            Console.Write("Введіть сумму поповнення: ");
            double summ = CheckNumberDouble();
            return (LicensePlate,summ);
        }
        static public double CheckNumberDouble()
        {
            double i = 0;
            try
            {
                i = double.Parse(Console.ReadLine());
            }
            catch
            {
                Console.WriteLine("Не вірно введено номер, спробуйте ще раз: ");
            }
            return i;
        }
    }
}
