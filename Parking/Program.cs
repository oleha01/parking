﻿using System;
using System.IO;
using System.Threading;
using ParkingLib.Class;

namespace Parking
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.OutputEncoding = System.Text.Encoding.Default;
            (new Thread(() =>
            {
                while (true)
                {
                    Thread.Sleep(10000);
                    var trans = SingleParking.getInstance().Transactions;
                    //Console.WriteLine("Clear");
                    using (FileStream fs = new FileStream("Transactions.log", FileMode.Append))
                    {
                        using (StreamWriter sr = new StreamWriter(fs))
                        {
                            foreach (var el in trans)
                            {
                                sr.WriteLine("{0} {1} {2} {3};", el.Date, el.LicensePlate, el.CarType, el.Amount);
                            }
                        }
                    }
                    trans.Clear();


                }

            })).Start();
            //(new Thread(() =>
            //{
            //    while (true)
            //    {
            //        Thread.Sleep(Setting.Periodicity * 1000);
            //        var cars = SingleParking.getInstance().Cars;
            //        var trans = SingleParking.getInstance().Transactions;
            //        //Console.WriteLine("Trans");
            //        foreach (var el in cars)
            //        {
            //            double summ = Setting.getPrice(el.CarType);
            //            if (summ > el.Balance)
            //            {
            //                summ *= Setting.Fine;
            //            }
            //            el.MinusBalance(summ);
            //            SingleParking.getInstance().Balance += summ;
            //            trans.Add(new Transaction(el.CarType, summ, el.LicensePlate));
            //        }
            //    }

            //})).Start();
            while (true)
            {
                int i = Menu.MainMenu();
                /*"1.Дізнатися поточний баланс Парковки\n";
            s += "2.Суму зароблених коштів за останню хвилину.\n";
            s += "3.Дізнатися кількість вільних/зайнятих місць на парковці.\n";
            s += "4.Вивести на екран усі Транзакції Праковки за останню хвилину\n";
            s += "5.Вивести усю історію Транзакцій\n";
            s += "6.Вивести на екран список усіх Транспортних засобів \n";
            s += "7.Створити/поставити Транспортний засіб на Парковку\n";
            s += "8.Видалити/забрати Транспортний засіб з Парковки\n";
            s += "9.Поповнити баланс конкретного Транспортного засобу\n";*/
                switch (i)
                {
                    case 1: Executing.CurrentBalance(); break;
                    case 2: Executing.SummMoney();  break;
                    case 3: Executing.CountPlace(); break;
                    case 4: Executing.LastTrans(); break;
                    case 5: Executing.AllTrans();  break;
                    case 6: Executing.ShowAllCars(); break;
                    case 7: Executing.IfPlace(); break;
                    case 8: Executing.DeleteCar(Menu.DeleteCarMenu()); break;
                    case 9: Executing.AddBalance(Menu.AddBalance()); break;
                }
                Console.WriteLine();
                Console.WriteLine("<<<Натисніть будь-яку кнопку, щоб перейти в головне меню>>>");
                Console.ReadKey();
            }
        }
    }
}
