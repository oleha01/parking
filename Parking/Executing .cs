﻿using System;
using System.Collections.Generic;
using System.Text;
using ParkingLib.Class;
using System.Threading;
using System.IO;

namespace Parking
{
    public static class Executing
    {
        static Dictionary<string, Thread> thread;
        static Dictionary<string, bool> threadStop;
        static Executing()
        {
            thread = new Dictionary<string, Thread>();
            threadStop = new Dictionary<string, bool>();
        }
        public static void CurrentBalance()
        {
            Console.Clear();
            Console.Write("Баланс парковки: ");
            Console.WriteLine(SingleParking.getInstance().Balance);
        }
        public static void CreateCar((string, int, double) newCar)
        {
            Console.Clear();
            if (newCar.Item2 < 5 && newCar.Item2 > 0)
            {
                Car car = new Car(newCar.Item3, (CarEnum)newCar.Item2, newCar.Item1);
                threadStop.Add(car.LicensePlate, true);
                Thread th = (new Thread(() =>
                {
                    while (threadStop[car.LicensePlate])
                    {
                        Thread.Sleep(Setting.Periodicity * 1000);
                        if (threadStop[car.LicensePlate] == false)
                            break;
                        var trans = SingleParking.getInstance().Transactions;
                        //Console.WriteLine("Trans");
                            double summ = Setting.getPrice(car.CarType);
                            if (summ > car.Balance)
                            {
                                summ *= Setting.Fine;
                            }
                            car.MinusBalance(summ);
                            SingleParking.getInstance().Balance += summ;
                            trans.Add(new Transaction(car.CarType, summ, car.LicensePlate));
                    }
                    threadStop.Remove(car.LicensePlate);

                }));
                th.Start();
                thread.Add(newCar.Item1, th);
                SingleParking.getInstance().Cars.Add(car);
                Console.WriteLine("Додано автомобіль: Номер:{0}, Тип:{1}, Баланс:{2}", car.LicensePlate, car.CarType, car.Balance);
            }
            else
            {
                Console.WriteLine("Не вірно введено дані!!!");
            }
        }
        public static void DeleteCar(string number)
        {
            Console.Clear();
            Car car = SingleParking.getInstance().Cars.Find(c => c.LicensePlate == number);
            if (car != null)
            {
                Console.WriteLine("Знайдено автомобіль: Номер:{0}, Тип:{1}, Баланс:{2}", car.LicensePlate, car.CarType, car.Balance);
                if (car.Balance < 0)
                    Console.WriteLine("!!!!!!!!!Потрібна доплата: {0}$!!!!!!!!!", Math.Abs(car.Balance));
                Console.WriteLine("Ви дійсно бажаєте видалити автомобіль з номерним знаком: {0}?", car.LicensePlate);
                Console.WriteLine("1.Так");
                Console.WriteLine("2.Ні");
                int i = Menu.CheckNumber();
                if (i == 1)
                {
                    threadStop[car.LicensePlate] = false;
                    thread.Remove(car.LicensePlate);
                    SingleParking.getInstance().Cars.Remove(car);
                    Console.WriteLine("Видалено автомобіль: Номер:{0}, Тип:{1}, Баланс:{2}", car.LicensePlate, car.CarType, car.Balance);
                }
            }
            else
            Console.WriteLine("Автомобіль з номерним знаком: {0} не знайдено!!!", number);
        }
        public static void ShowAllCars()
        {
            Console.Clear();
            var cars = SingleParking.getInstance().Cars;
            foreach (var el in cars)
            {
                Console.WriteLine("Номер:{0}, Тип:{1}, Баланс:{2}", el.LicensePlate, el.CarType, el.Balance);
            }
        }
        public static void SummMoney()
        {
            Console.Clear();
            var trans = SingleParking.getInstance().Transactions;
            double summ = 0.0;
            foreach (var el in trans)
            {
                summ += el.Amount;
            }
            Console.WriteLine("За останню хвилино було проведено транзакцій на сумму: {0}", summ);
        }
        public static void CountPlace()
        {
            Console.Clear();
            var park = SingleParking.getInstance();
            Console.WriteLine("Загальна кількість: {0}\nКількість вільних місць: {1}\nКількість зайнятих місць:{2}", park.Capacity, park.Capacity - park.Cars.Count, park.Cars.Count);
        }
        public static void LastTrans()
        {
            Console.Clear();
            var trans = SingleParking.getInstance().Transactions;
            foreach (var el in trans)
            {
                Console.WriteLine("{0} {1} {2} {3}", el.Date,el.LicensePlate, el.CarType, el.Amount);
            }
        }
        public static void AllTrans()
        {
            Console.Clear();
            using (StreamReader sr = new StreamReader("Transactions.log"))
            {
                string log = sr.ReadToEnd();
                string[] rows = log.Split(';');
                foreach(var el in rows)
                {
                    string[] elements = el.Split(' ');
                    try
                    {
                        Console.WriteLine("{0} {1} {2} {3} {4}", elements[0], elements[1], elements[2], elements[3],elements[4]);
                    }
                    catch
                    {

                    }
                }
            }
        }
        public static void AddBalance((string,double) findCar)
        {
            Console.Clear();
            Car car = SingleParking.getInstance().Cars.Find(c => c.LicensePlate == findCar.Item1);
            if(car!=null)
            {
                car.Balance += findCar.Item2;
                Console.WriteLine("Номер:{0}, Тип:{1}, Баланс:{2}", car.LicensePlate, car.CarType, car.Balance);
            }
            else
            {
                Console.WriteLine("Автомобіль з номерним знаком: {0} не знайдено!!!", findCar.Item1);
            }
        }
        public static void IfPlace()
        {
            if(SingleParking.getInstance().Capacity > SingleParking.getInstance().Cars.Count)
            Executing.CreateCar(Menu.CreateCarMenu());
            else
                Console.WriteLine("!!!Немає вільних місць!!!");
        }
    }
}
